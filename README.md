bootstrap
=============
server configuration standard USAePay PCI DSS

Summary
-------------
A command line tool for configuring server base for USAePay PCI DSS.  
http://mirror/bootstrap.txt

Usage
-------------
Command Line: 

    $ bootstrap --help 
    
Usage Remote
-------------
Command Line: 

    $ curl -s http://mirror/bootstrap.txt | python 
    
Command Line passing options:

    $ curl -s http://mirror/bootstrap.txt | python /dev/stdin -o ldap
    
Standard Optons:

    You can provide a hostname. If not, the script will use the current hostname.
    
    Standard hostname convention is: type-datacenter-number
    For example, the second database server in datacenter ca1:
    
    db-ca1-02
 
    curl -s http://mirror/bootstrap.txt | python /dev/stdin db-ca1-01

    Rather than run the full script, use one of the following options:

            hosts       - sets the hosts file
            motd        - sets the motd
            ipv6        - disables ipv6
            yum         - sets the yum repo
            rpms        - installs base rpms
            ntp         - sets ntp
            ldap        - sets ldap client config
            sudo        - sets sudoers
            ssh         - sets ssh files
            services    - sets services
            mail        - sets mail host
            php         - sets php.ini
            syslog      - sets syslog
            iptables    - sets iptables
            pamaccess   - enables /etc/security/access.conf
            monitor     - sets monitor

    These do not auto run:
            netsnmp     - sets snmp client
            swapfile    - sets /swapfile
            proute      - sets private backend route
            fchost      - installs fibre channel 
            mysqlserver - installs mysql server

    curl -s http://mirror/bootstrap.txt | python /dev/stdin -o ldap





   

 


